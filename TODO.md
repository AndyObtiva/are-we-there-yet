# TODO

Add miscellaneous tasks and issues here.

- Default start date to that right after the last task in the project
- Fix issue with selection/deletion on Windows
- Add a today button to center Gantt Chart
- Remove task via Gantt Chart GUI
- Have an archived task tab
- Fix form field alignment with table columns
- Add a menu with theme configuration as an option
- Attempt to add ? to the end of name in Mac app
